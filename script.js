'use strict';

debugger

//Iteración 1

    var myFavoriteHero = 'Hulk';
    var x = 50;
    var h = 5;
    var y = 10;
    var z = h + y;

//Iteración 2

    //1.1
    const character = {name: 'Jack Sparrow', age: 10};

    character.age = 25;

    console.log(character);

    //1.2

    let firstName = 'Jon';
    let lastName = 'Snow';
    let age = '24';

    console.log('Soy ' + firstName + ' ' + lastName + ', tengo ' + age + ' años y me gustan los lobos.');

    //1.3

    const toy1 = {name: 'Buss myYear', price: 19};
    const toy2 = {name: 'Rallo mcKing', price: 29};

    let totalPrice = toy1.price + toy2.price;

    console.log(totalPrice);

    //1.4

    let globalBasePrice = 10000;
    const car1 = {name: 'BMW m&m', basePrice: 50000, finalPrice: 60000};
    const car2 = {name: 'Chevrolet Corbina', basePrice: 70000, finalPrice: 80000};

    globalBasePrice = 25000;
    car1.finalPrice = globalBasePrice + car1.basePrice;
    car2.finalPrice = globalBasePrice + car2.basePrice;

    console.log(car1);
    console.log(car2);

// Iteración 3

    //1.1

    let operation1 = 10 * 5;

    console.log(operation1);


    //1.2

    let operation2 = 10 / 2;

    console.log(operation2);

    //1.3

    let operation3 = 15 * 9;

    console.log(operation3);

    //1.4

    var y = 10;
    var z = 5;
    var x;

    x = y + z;
    console.log(x);

    //1.5

    x = y * z;
    console.log(x);

// Iteración 4

    //1.1

    const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];

    console.log(avengers[0]);

    //1.2

    avengers[0] = 'IronMan';

    console.log(avengers[0]);

    //1.3

    console.log(avengers.length);

    //1.4

    const rickAndMortyCharacters = ["Rick", "Beth", "Jerry"];

    rickAndMortyCharacters.push('Morty', 'Summer');

    console.log(rickAndMortyCharacters[rickAndMortyCharacters.length - 1]);

    //1.5
    console.log(rickAndMortyCharacters);
    //1.6

    rickAndMortyCharacters.push('Lapiz Lopez');

    rickAndMortyCharacters.splice(1, 1);
    console.log(rickAndMortyCharacters);

//Iteración 5

    const number1 = 10;
    const number2 = 20;
    const number3 = 2;

    if(number1 === 10){
        console.log('number1 es estrictamente igual a 10')
    }

    if (number2 / number1 == 2) {
    console.log("number2 dividido entre number1 es igual a 2");
    }

    if (number1 !== number2) {
    console.log("number1 es estrictamente distinto a number2");
    }

    if (number3 != number1) {
    console.log("number3 es distinto number1");
    }

    if (number3 * 5 == number1) {
    console.log("number3 por 5 es igual a number1");
    }

    if (number3 * 5 == number1 && number1 * 2 == number2) {
    console.log("number3 por 5 es igual a number1 Y number1 por 2 es igual a number2");
    }

    if (number2 / 2 == number1 || number1 / 5 == number3) {
    console.log("number2 entre 2 es igual a number1 O number1 entre 5 es igual a number3");
    }





